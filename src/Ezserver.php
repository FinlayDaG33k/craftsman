<?php
  namespace Craftsman;

  class EzServer { 
    public function JsLog($message) {
      ?>
        <script>console.log(`<?= $message; ?>`);</script>
      <?php
      return;
    }

    /**
    * Get the protocol of the request
    * Compatible with Cloudflare
    *
    * @return string Wether using HTTP or HTTPS
    */
    public function GetProto() {
      // Check wether is using Cloudflare or not
      
      if(!empty($_SERVER['HTTP_CF_VISITOR'])){
        return json_decode($_SERVER['HTTP_CF_VISITOR'],1)['scheme'];
      }else{
        return (!empty($_SERVER['HTTPS'])) ? "https" : "http";
      }
    }

    /**
    * The method through which the request was send
    *
    * @return mixed The method
    */
    public function GetMethod(){
      switch(strtoupper($_SERVER['REQUEST_METHOD'])){
        case "GET":
          return "GET";
          break;
        case "HEAD":
          return "PUT";
          break;
        case "POST":
          return "POST";
          break;
        case "PUT":
          return "PUT";
          break;
        case "DELETE":
          return "DELETE";
          break;
        case "CONNECT":
          return "CONNECT";
          break;
        case "OPTIONS":
          return "OPTIONS";
          break;
        case "TRACE":
          return "TRACE";
          break;
        case "PATCH":
          return "PATCH";
          break;
        default:
          return "UNKNOWN";
          break;
      }
    }

    /**
    * @return string The home path of the current file
    */
    public function GetHome(){
      $currentPath = $_SERVER['PHP_SELF'];
      $pathInfo = pathinfo($currentPath);
      return rtrim($_SERVER['HTTP_HOST'].$pathInfo['dirname'],"/");
    }

    /**
    * @return string The root of the current domain
    */
    public function GetRoot(){
      return $_SERVER['HTTP_HOST'];
    }

    /**
    * Check wether the page the user requested exists
    *
    * @param string $pageDir The directory in which to check for pages
    * @param string $defaultPage The page to which to default when no page is specified
    * @param string $errorPage The page to show when the requested page does not exist
    * @return string The page to show the user
    */
    public function GetPage($pageVar = "page", $pageDir = "pages", $defaultPage = "home", $errorPage = "404"){
      if (!empty($_GET[$pageVar])) { // Check if the user explicitly requested a page
        $tmp_page = basename($_GET[$pageVar]); // Put the requested pagename into a little variable
        if (file_exists("{$pageDir}/{$tmp_page}.php")) { // Check if the page the user requested exists in out /pages directory
          // If the file exists
          return $tmp_page; // return the tmp_page as the page
        } else {
          // If the page does not exist
          return $errorPage; // return the errorpage as the page
        }
      }else{
        // If there has no page been explicitly requested by the user
        return $defaultPage; // return the homepage as the page
      }
    }

    /**
    * Generate a pseudo-random string
    * Use with PHP >= 7.0.0 for best results!
    *
    * @param int $length The length of the random string
    * @return string The random string
    */
    public function RandomStr($length = 8){
      // Use the random_bytes(), it should know what's best for us
      $bytes = random_bytes($length / 2); // Generate random bytes
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $result = '';
      // Split the string of random bytes into individual characters
      foreach (str_split($bytes) as $byte) {
        $result .= $characters[ord($byte) % $length];
      }
      return $result;
    }

    /**
    * Insert a string inside another string
    *
    * @param string $insert What to insert
    * @param int $pos the position where to insert
    * @param string $string The starting string
    * @return string The resulting string
    */
    public function StrInsert($insert, $pos, $string){
      return substr($string, 0, $pos) . $insert . substr($string,0, $pos + strlen($insert));
    }

    /**
    * Check a misspelled word for the closest suggestion
    *
    * @param string $input The input word
    * @param array $wordlist An array containing all words to check the $input against
    * @return array An array containing the closest word and their distance
    */
    public function Levenshtein($input,$words){
      $shortest = -1;
      foreach ($words as $word) {
        // calculate the distance between the input word and the current word
        $lev = levenshtein($input, $word);
        // check for an exact match
        if ($lev == 0) {
          // closest word is this one (exact match!)
          $closest = $word;
          $shortest = 0;
          // We've found an exact match, break the loop
          break;
        }
        // if this distance is less than the next found shortest distance, OR if a next shortest word has not yet been found
        if ($lev <= $shortest || $shortest < 0) {
          // set the closest match and shortest distance
          $closest  = $word;
          $shortest = $lev;
        }
      }
      // give the output to back
      return array("closest" => $closest, "distance" => $shortest);
    }

    /**
    * Get the visitor IP (Cloudflare compatible)
    *
    * @param bool $overwriteGlobal wether to overwrite the $_SERVER['REMOTE_ADDR'] global on detection of Cloudflare
    * @return string The visitor IP
    */
    public function GetVisitorIP($overwriteGlobal = false){
      if(!empty($_SERVER['HTTP_CF_CONNECTING_IP'])){
        if($overwriteGlobal){
          $_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_CF_CONNECTING_IP'];
        }
        return $_SERVER['HTTP_CF_CONNECTING_IP'];
      }else{
        return $_SERVER["REMOTE_ADDR"];
      }
    }
  }