<?php
  namespace Craftsman;

  class Files {
    /**
    * Convert bytecount to a more human-readable format.
    *
    * @param int $bytes The bytes to convert
    * @param array $units An array of string objects, containing the display values
    * @return string The human-readable format
    */
    public function IntToBinary($bytes,$units,$precision = 2){
      $bytes = max($bytes, 0);
      $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
      $pow = min($pow, count($units) - 1);
      $bytes /= pow(1024, $pow);
      return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
    * Convert bytecount to a more human-readable format.
    *
    * @param int $bytes The bytes to convert
    * @param array $units An array of string objects, containing the display values
    * @return string The human-readable format
    */
    public function IntToSI($bytes,$units,$precision = 2){
      $bytes = max($bytes, 0);
      $pow = floor(($bytes ? log($bytes) : 0) / log(1000));
      $pow = min($pow, count($units) - 1);
      $bytes /= pow(1000, $pow);
      return round($bytes, $precision) . ' ' . $units[$pow];
    }
  }